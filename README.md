

As you can guess, this is not a super-serious high-value-delivering project.

This project is mainly here for me to fart around with comparing Scala's FP v.s. OOP features, see how I'd implement some type hierarchy, IO, and state management features in the context of a text-based adventure game.

And yes, I realize the JVM start-up time sucks and is not ideal for an adventure game, but the point was to see how I'd model problems.
