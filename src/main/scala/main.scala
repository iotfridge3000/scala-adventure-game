import scala.Option;
import scala.util._;
//import scala.collections._;

class Party {

}

trait Feels {
  def attitudes: Map[String[String]]
  def karma: Int
  def ask(): String
}

/*
class NPC extends Feels {
  name = ""
  override def ask() = {
    println("What did you want to ask s'name' about?")
  }
  override def karma = 100
  override def attitudes:
}
*/





object Main {

  sealed case class NPC(name: String, race: String, job: String, hp: Int)

  //convert this to Option/Try/Either instead?
  /*
  def isDead(person: NPC): Boolean = person.hp match {
    case > 0 => false
    case <= 0 => true
    case _ => true
  }
*/

  def promptFight(): Unit = {
    println("You looking to start something?")
    val response = readLine()
    if (response != "yes") {
      println("That's what I thought, you fucking punk. Now beat it.")
    }
  }

  def ejectPatron(): Unit = {
      println("Get out and don't come back until you know what you want!")

  }
  
  def checkInventoryFor(): Boolean = {
    true
  }
  
  def checkPartyForEntity(party: Vector[NPC], entityType: String): Boolean ={
    val inParty = party.map(_.job == entityType) 
    true
  }

  def promptWithAvailableMoves() = {
    println("leave")
    println("talk")

    val command = readLine()

  }

  def checkpoint() = {

    println("The guard stares down at the clipboard. I think you're good to go. It doesn't look like any of your names are on here")
  }

  def startInvasionCounter() {

  }

  def addPartyMember() {
    //do a charisma check and see what the max # of party members is
    println("Look, wise guy. You can't have more than 8 party members. It's not like you're a cult leader or something.")

  }

  def check_wallet() {
    if {wallet < 0) {
      "You can't afford anyhing now. Maybe we should come back later."
    }
  }

  def main(args: Array[String]) = {
    val daysLeft: int = 100

    println("What class do you want to play as?")
    println("A wizard")
    println("A medic")
    println("A codemonkey")

    val Gerry = NPC("Gerry","Martian", "Wizard", 100)
    val party: Vector[NPC] = Vector(Gerry)
    val action = readLine()

    action match {
      case "fight" => promptFight()
      case _ => ejectPatron()
    }
  }

}
